# kasm-test

kasm test image with SublimeText installed, following the tutorial on the Kasm official website (build custom image).

## local usage

docker run -it --rm --shm-size=512m -p 6901:6901 -e VNC_PW=password registry.gitlab.com/therickys93/kasm-test:sublimetext

then visit http://ip-address:6901

